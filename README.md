# weather-app-master

The project gives you the weather forecast for the next five days using API http://openweathermap.org/.

Some screenshots of this weather-app are:

![](images/screenshots/weather-app-ss1.JPG)
![](images/screenshots/weather-app-ss2.JPG)