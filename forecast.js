//http://api.openweathermap.org/data/2.5/forecast?q=mumbai&units=metric&APPID=e37ed0daef9e744119acf6e8646f96c4

const key = "e37ed0daef9e744119acf6e8646f96c4";

const getForecast = async (city) => {
    const base = "http://api.openweathermap.org/data/2.5/forecast";
    const query = `?q=${city}&units=metric&APPID=${key}`;
    
    const response = await fetch(base+query);
    //console.log(response);
    if(response.ok){
        const data = await response.json();
        return data;
    }else{
        throw new Error('Error Status: ' + response.status);
    }
}

//getForecast()
//    .then(data=>console.log(data))
//    .catch(err=>console.log(err));